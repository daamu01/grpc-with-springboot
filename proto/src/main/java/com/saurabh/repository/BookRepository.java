package com.saurabh.repository;

import com.saurabh.model.GrpcBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<GrpcBook,Long> {
}