package com.saurabh;

import com.saurabh.model.GrpcAuthor;
import com.saurabh.model.GrpcBook;
import com.saurabh.repository.AuthorRepository;
import com.saurabh.repository.BookRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class TempDB {

    private static List<Author> staticAuthorsList = new ArrayList<>();
    private static List<Book> staticBooksList = new ArrayList<>();
    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;

    @Autowired
    public TempDB(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
    }

    public static List<Author> getAuthorsFromTempDb() {
        return staticAuthorsList;
    }

    public static List<Book> getBooksFromTempDb() {
        return staticBooksList;
    }

    @PostConstruct
    public void initStaticData() {
        // Load data from repositories and populate your static lists here
        List<GrpcAuthor> authorEntities = authorRepository.findAll();
        List<GrpcBook> bookEntities = bookRepository.findAll();

        // Convert and store data in your static lists
        staticAuthorsList = convertToAuthors(authorEntities);
        staticBooksList = convertToBooks(bookEntities);
    }

    // Utility methods to convert entities to gRPC messages
    private List<Author> convertToAuthors(List<GrpcAuthor> authorEntities) {
        List<Author> authors = new ArrayList<>();
        for (GrpcAuthor dbAuthor : authorEntities) {
            Author grpcAuthor = Author.newBuilder()
                    .setAuthorId(dbAuthor.getAuthorId())
                    .setFirstName(dbAuthor.getFirstName())
                    .setLastName(dbAuthor.getLastName())
                    .setGender(dbAuthor.getGender())
                    .setBookId(dbAuthor.getBookId())
                    .build();
            authors.add(grpcAuthor);
        }
        return authors;
    }

    private List<Book> convertToBooks(List<GrpcBook> bookEntities) {
        List<Book> books = new ArrayList<>();
        for (GrpcBook book : bookEntities) {
            Book grpcBook = Book.newBuilder()
                    .setBookId(book.getBookId())
                    .setAuthorId(book.getAuthorId())
                    .setTitle(book.getTitle())
                    .setPrice(book.getPrice())
                    .setPages(book.getPages())
                    .build();
            books.add(grpcBook);
        }
        return books;
    }
}
